# README #

This is a standalone application written for Linux to calculate the evolution of the axes in the anisotropic universe picture.
The file aniso_scale.cc contains all the algorithm used to compute the evolution.

### INSTALLATION ###
Its simple, just run make and make sure you have all the libraries.  
Mainly this is gsl which might not be standard. See below.

### NEEDED LIBS ###
Except for gsl there is no other non-standard library necessary.
If you do not already have it you can find it here:
https://www.gnu.org/software/gsl/

### USAGE ###
The makefile will produce a executable named anisotropic_scale.
This file takes a few arguments.
Using it without any will result in an error but will also print the arguments you can put in.
For most of them there are some standards defined which are printed after the start message.

The arguments are
N (int):              Number of points on the time line
Lambda[0..2] (float): Eigenvalues of the tidal field. That are 3 values here.
aini (float):         Initial scale factor in the background cosmology
aend (float):         Final scale factor in the background cosmology

If you use this program for a published work, please cite 2018MNRAS.479..162S
### Known issues ###
For aend you should allow some buffer so that gsl is able to interpolate properly.
So if you want to go to a = 1 you should use an aend = 1.1 or so. Just ignore the values later on.
Searching for the issue was not a priority.

If you find problems, please use the issues tab and open a ticket or email me at aschmidt@mpa-garching.mpg.de.
Thank you.
