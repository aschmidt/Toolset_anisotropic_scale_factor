/*******************************************************************************
 ** \copyright   This file is a standalone version to compute the evolution
 ** \copyright   of anisotropic expansion factors. Copyright (C) 2018 by Andreas Schmidt
 ** \copyright   (aschmidt@mpa-garching.mpg.de)
 ** \copyright   If you use this code please cite 2018MNRAS.479..162S
 ********************************************************************************/

/**
 * This file is part of aniso_scale.

    aniso_scale is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    aniso_scale is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with aniso_scale.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <gsl/gsl_spline.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_odeiv.h>
#include <vector>

using namespace std;

/**
  * Function to calculate the growth factor in the limit of LambdaCDM
  * use gsl interpolation gsl_interp_linear
  *
  */
#define DI_TABLE_LENGTH   10000
#define HUBBLE            3.2407789e-18   /* in h/sec */
#define KILOPARSEC        3.085678e21     /* kpc in cm */
#define VELOCITYUNIT      1.e5             /* km/s in cm/s */


static double Hubble0 = HUBBLE * KILOPARSEC/VELOCITYUNIT;  /* Hubble in internal gadget units = 0.1 */
static double Omega0 = 0.3, OmegaLambda = 0.7;

/*######################### FUCTION DEFINITIONS ##################################*/

double hubble_function(double a)
{
  double hubble_a;

  hubble_a = Omega0 / (a * a * a) + (1 - Omega0 - OmegaLambda) / (a * a) + OmegaLambda;

  hubble_a = Hubble0 * sqrt(hubble_a);

  return hubble_a;
}


/**
  * Define functions and variables for the growthfactor integration
  */

gsl_spline *growth_interp;
gsl_interp_accel *growth_acc;
gsl_spline *delta_interp_spc;
gsl_interp_accel *delta_acc_spc;

/* Scaleing of the Hubble parameter */
// H(a) = E(a)*H0; E(a) =EA
double EA(double a)
{
    return sqrt(Omega0 / (a * a * a) + (1 - Omega0 - OmegaLambda) / (a * a) + OmegaLambda);
}

/**
  * This is the integrant for the growthfactor integration which is done below
  */
static double growth_integrant(double a, void *param)
{
  if(a < 1e-10)
    return pow(a, 3./2.);
  double ea = EA(a);
  return (1./(a*a*a*ea*ea*ea));
}

/**
  * Initialization for the integration of D(a)
  * First value is set to 0
  * The integration is performed using gsl and stored in the gsl_spline growth_interp
  * We store 1000 points in a
  */

void init_growth(double time_end, double Omega0)
{
  #define WORKSIZE 100000
  double result, abserr;
  gsl_function F;
  gsl_integration_workspace *workspace;
  workspace = gsl_integration_workspace_alloc(WORKSIZE);
  double Di[DI_TABLE_LENGTH];
  double ai[DI_TABLE_LENGTH];

  /* do integration and save results into ai, Di */
  ai[0] = 0.;
  Di[0] = 0.;
  for(int i = 1; i < DI_TABLE_LENGTH; i++)
    {
      F.function = &growth_integrant;
      ai[i] = time_end / (DI_TABLE_LENGTH-1) * i;
      gsl_integration_qag(&F, 0., ai[i], 0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
      Di[i] = 5./2. * Omega0 * EA(ai[i]) * result;
      //printf("i = %d ai = %g, Di = %g\n", i, ai[i], Di[i]);
    }

  gsl_integration_workspace_free(workspace);

  growth_acc = gsl_interp_accel_alloc();
  growth_interp = gsl_spline_alloc(gsl_interp_cspline, DI_TABLE_LENGTH);
  gsl_spline_init(growth_interp, ai, Di, DI_TABLE_LENGTH);
}

/*
 * Free the integration variables and workspace
 */
void free_growth()
{
  gsl_spline_free(growth_interp);
  gsl_interp_accel_free(growth_acc);
}

/**
  * This function does the interpolation between different a values using a spline.
  */
double Growth(double a)
{
  return gsl_spline_eval(growth_interp, a, growth_acc);
}

/**
  * This calculated the derivative of D with respect to a
  */
double dGrowth_da(double a)
{ //gsl_spline_eval_deriv (const gsl_spline * spline, double x, gsl_interp_accel * acc)
  return gsl_spline_eval_deriv(growth_interp, a, growth_acc);
}

/*######################## SPHERICAL COLLAPSE #####################################*/
/* Following the python code from Jens Stuecker */

double Kp(double delta, double aini)
{
  double deltaini = delta * Growth(aini)/Growth(1.);
  //printf("deltaini = %g, Growth(%g) = %g, Kp = %g\n", deltaini, aini, Growth(aini),5./3. * Omega0 * (deltaini/aini));
  return( 5./3. * Omega0 * (deltaini/aini));
}

double Hp(double delta, double a, double ap, double kp)
{
  double Omegap = Omega0 * (1.+delta)/(a * a * a);
  double hubble2 = Hubble0*Hubble0 * (Omegap + OmegaLambda) - kp/(ap*ap);
  //printf("Hp -> Omegap = %g, kp = %g, delta = %g, a = %g, ap = %g, Hp^2 = %g\n", Omegap, kp, delta, a, ap, hubble2);
  if(hubble2 > 0)
    return(sqrt(hubble2));
  else
    return(-sqrt(fabs(hubble2)));
}

gsl_spline *growth_interp_spc;
gsl_interp_accel *growth_acc_spc;

void initial_diff_spc(const double aini, double * y, double delta0)
{
  y[0] = aini;
  y[1] = aini;
  y[2] = delta0 * Growth(aini)/Growth(1.);
  y[3] = hubble_function(aini) * delta0 * dGrowth_da(aini) * aini / Growth(1.);
  printf("Initial values = %g %g %g %g\n", y[0], y[1], y[2], y[3]);
}

int spherical_collapse_ode(double aend, const double y[], double f[], void *params)
{
  double a = y[0];
  double ap = y[1];
  double delta = y[2];
  double deltadot = y[3];
  double kp = *(double *) params;

  double da_dt = hubble_function(a) * a;
  double dap_dt = Hp(delta, a, ap, kp) * ap;

  double ddelta_dt = deltadot;
  double ddeltadot_dt = 3./2. * Omega0 /(a*a*a) * Hubble0 * Hubble0 * (1.+delta) * delta - 2. * hubble_function(a) * deltadot + 4./3.* deltadot * deltadot /(1.+delta);
  f[0] = da_dt;
  f[1] = dap_dt;
  f[2] = ddelta_dt;
  f[3] = ddeltadot_dt;

  return GSL_SUCCESS;
}

void do_differential_spc(const double aini, const double delta0, void * da_params)
{
  // We store the DI from the ODE in one splines so we can interpolate using gsl
  int dimension;
  dimension = 4;          // number of differential equations

  double eps_abs = 1.e-8;     // absolute error
  double eps_rel = 1.e-10;    // relativ error

  // define the type of routine to make the ode integration
  const gsl_odeiv_step_type * type_ptr = gsl_odeiv_step_rkf45;
  // allocate and initialize the functions from gsl (stepper, control, evolution)
  gsl_odeiv_step *step_ptr = gsl_odeiv_step_alloc(type_ptr, dimension);
  gsl_odeiv_control *control_ptr = gsl_odeiv_control_y_new(eps_abs, eps_rel);
  gsl_odeiv_evolve *evolve_ptr = gsl_odeiv_evolve_alloc(dimension);

  gsl_odeiv_system ode_system;    // structure with the functions
  double params;
  double y[dimension];
  double t, t_next, tmin, tmax, delta_t;
  double h = 1e-6;

  //Now load the values into the ode_system
  ode_system.function = spherical_collapse_ode;
  ode_system.jacobian = NULL;
  ode_system.dimension = dimension;

  //Initialize values
  t = tmin = 0;
  //delta_t = (tmax - tmin)/N;
  delta_t = 0.0001; // To get the same steps as in the approximation

  printf("Using tstart = %g, tend = %g -> delta t = %g, N = %d\n", tmin, tmax, delta_t, (int)((tmax-tmin)/delta_t));
  initial_diff_spc(aini, &y[0], delta0); //initial values
  params = Kp(delta0, y[0]); // delta, aini
  // Extra paramteres
  ode_system.params = &params;
  double deltaini = delta0 * Growth(aini)/Growth(1.);
  FILE *fp_ode;
  char filebase2[] = "./SPHERICAL_ODE_Delta_";

  char buf2[200];
  sprintf(buf2, "%s%.2f", filebase2, delta0);
  fp_ode = fopen(buf2,"w");
  fprintf(fp_ode, "# t \t a \t ap \t delta \t D \t deltadot \n");
  fprintf(fp_ode, "%g %g %g %g %g %g \n", t, y[0], y[1], y[2], y[2] * aini / deltaini, y[3]);
  // Initialize spline
  growth_acc_spc = gsl_interp_accel_alloc();
  delta_acc_spc = gsl_interp_accel_alloc();

  vector<double> ai;
  vector<double> Di;
  vector<double> delta;
  ai.push_back(y[0]);
  Di.push_back(y[2] * aini / deltaini);
  delta.push_back(y[2]);
  // Now do the stepping
  for (t_next = tmin + delta_t; y[0] <= 1.; t_next += delta_t)
  {
    while(t < t_next)
    {
      gsl_odeiv_evolve_apply(evolve_ptr, control_ptr, step_ptr, &ode_system, &t, t_next, &h, y);
    }
    fprintf(fp_ode, "%g %g %g %g %g %g \n", t, y[0], y[1], y[2], y[2] * aini / deltaini, y[3]);
    ai.push_back(y[0]);
    Di.push_back(y[2] * aini / deltaini);
    delta.push_back(y[2]);
  }
  /* all done; free up the gsl_odeiv stuff */
  growth_interp_spc = gsl_spline_alloc(gsl_interp_cspline, int(ai.size()));
  delta_interp_spc = gsl_spline_alloc(gsl_interp_cspline, int(ai.size()));
  gsl_spline_init(growth_interp_spc, &(ai.front()), &(Di.front()), int(ai.size()));
  gsl_spline_init(delta_interp_spc, &(ai.front()), &(delta.front()), int(ai.size()));

  gsl_odeiv_evolve_free (evolve_ptr);
  gsl_odeiv_control_free (control_ptr);
  gsl_odeiv_step_free (step_ptr);
  fclose(fp_ode);
}


void free_spline()
{
  gsl_spline_free(growth_interp_spc);
  gsl_interp_accel_free(growth_acc_spc);
  gsl_spline_free(delta_interp_spc);
  gsl_interp_accel_free(delta_acc_spc);
}

double Growth_spc(double a)
{
  return gsl_spline_eval(growth_interp_spc, a, growth_acc_spc);
}

double dGrowth_da_spc(double a)
{ //gsl_spline_eval_deriv (const gsl_spline * spline, double x, gsl_interp_accel * acc)
  return gsl_spline_eval_deriv(growth_interp_spc, a, growth_acc_spc);
}

double delta(double a)
{
  return gsl_spline_eval(delta_interp_spc, a, delta_acc_spc);
}

/**
 *   anisotropic Scalefactor should scale with the linear growth factors which should be anisotropic as well
 *   PARAM:  a = background scale factor
 *           Lambda = Eigenvalues of the tidal tensor
 *           * dist = pointer where the values should be stored.
 */
void calc_scale_spherical(double a, const double Lambda[3], double * dist)
{
 double growth_now = Growth_spc(1.);
 //printf("Growthfactor today %g\n", growth_now);
 // All.Time = Background cosmological scale factor
 for(int i = 0; i < 3; i++)
 {
   dist[i] = pow(delta(a)+1, -1./3.);
   //dist[i] = 1. - Growth_spc(a)/growth_now * Lambda[i]; // Lambda = Eigenvalue Deformation Tensor (Tidal field)
   dist[i+3] = -Lambda[i]/growth_now * dGrowth_da_spc(a)*a*a*a*hubble_function(a);
   //printf("a_bg = %g -> New scale factor on axis %d = %g Growth factor(a_bg) = %g Lambda = %g\n",
   //        a, i, dist[i], Growth(a), Lambda[i]);
 }
}

/*################################## END OF SPHERICAL COLLAPSE#############################################*/

/*##################### Aux helper function ###############################*/
void get_name(const double Lambda[3], int N, const char filebase[], char buf[200])
{
  int temp_lambda[3] = { (int)(Lambda[0]*100), (int)(Lambda[1]*100), (int)(Lambda[2]*100) };
  printf("%s -> %d%d%d\n", filebase, temp_lambda[0], temp_lambda[1], temp_lambda[2]);
  if(Lambda[0] >= 0 && Lambda[1] >= 0 && Lambda[2] >= 0)
    sprintf(buf,"%sp%dp%dp%d_N_%d.txt", filebase, temp_lambda[0], temp_lambda[1], temp_lambda[2], N);

  else if(Lambda[0] >= 0 && Lambda[1] >= 0 && Lambda[2] < 0)
    sprintf(buf,"%sp%dp%dn%d_N_%d.txt", filebase, temp_lambda[0], temp_lambda[1], -temp_lambda[2], N);

  else if(Lambda[0] >= 0 && Lambda[1] < 0 && Lambda[2] >= 0)
    sprintf(buf,"%sp%dn%dp%d_N_%d.txt", filebase, temp_lambda[0], -temp_lambda[1], temp_lambda[2], N);

  else if(Lambda[0] < 0 && Lambda[1] >= 0 && Lambda[2] >= 0)
    sprintf(buf,"%sn%dp%dp%d_N_%d.txt", filebase, -temp_lambda[0], temp_lambda[1], temp_lambda[2], N);

  else if(Lambda[0] < 0 && Lambda[1] < 0 && Lambda[2] >= 0)
    sprintf(buf,"%sn%dn%dp%d_N_%d.txt", filebase, -temp_lambda[0], -temp_lambda[1], temp_lambda[2], N);

  else if(Lambda[0] >= 0 && Lambda[1] < 0 && Lambda[2] < 0)
    sprintf(buf,"%sp%dn%dn%d_N_%d.txt", filebase, temp_lambda[0], -temp_lambda[1], -temp_lambda[2], N);

  else if(Lambda[0] < 0 && Lambda[1] >= 0 && Lambda[2] < 0)
    sprintf(buf,"%sn%dp%dn%d_N_%d.txt", filebase, -temp_lambda[0], temp_lambda[1], -temp_lambda[2], N);

  else if(Lambda[0] < 0 && Lambda[1] < 0 && Lambda[2] < 0)
    sprintf(buf,"%sn%dn%dn%d_N_%d.txt", filebase, -temp_lambda[0], -temp_lambda[1], -temp_lambda[2], N);
}

/*################################ MAIN FUNCTION #########################################*/
// Run the main program to get the approximate value for x_i

int main(int argc, char *argv[])
{
  int N;  // Number of steps (a's) we test
  double Lambda[3] = {0.01, -0.03, 0.01}; // Initialize to a certain set of Eigenvalues
  double distortion[6];
  double dDgrowth;
  double aini = 1./(100.); // z = 99
  double delta0 = 1.686;

  printf("Running function %s with %d arguments\n", argv[0], argc);
  printf("Function %s has possible arguments: N (int), Lambda[0] (float), Lambda[1] (float), Lambda[2] (float), aini (float) aend (float)\n\n", argv[0]);

  if(argc == 1)
    N = 100;
  else if(argc > 1)
    N = atoi(argv[1]);

  if(argc >= 5)
  {
    Lambda[0] = atof(argv[2]);
    Lambda[1] = atof(argv[3]);
    Lambda[2] = atof(argv[4]);
  }
  if(argc >= 6)
    aini = atof(argv[5]); // should be the same as 1/N

  double aend = 1.;
  if(argc >=7)
    aend = atof(argv[6]);

  delta0 = Lambda[0] + Lambda[1] + Lambda[2]; //1./((1.- Lambda[0]) * (1.- Lambda[1]) * (1.- Lambda[2]));


  double a_test[N+1]; // This are the a values of the background cosmology we use to interpolate

  printf("Lambda input %g%g%g, delta = %g, Omega0 = %g, OmegaLambda = %g\n", Lambda[0], Lambda[1], Lambda[2], delta0, Omega0, OmegaLambda);
  char lambda_str[100];
	init_growth(aend, Omega0);
  // Now open a file to write to
  FILE *fp;
  const char filebase[] = "Spherical_collapse_";
  char buf[200];
  get_name(Lambda, N, &filebase[0], buf);

  printf("%s\n", buf);
  //sprintf(buf,"./background_cosmology_N_%d.txt", N);
  fp = fopen(buf,"w");
  fprintf(fp, "# i \t a_bg \t Distortion [3] \t p_i[3] \t Lambda[3] = (%g%g%g), dD/da\n", Lambda[0], Lambda[1], Lambda[2]);

  do_differential_spc(aini, delta0, NULL);

  // Main loop to do the interpolation
  for(int i = 0; i < N; i++)
  {
    a_test[i] = (1./N)*(i+1);
    //printf("Doing step %d of %d a = %g\n",i, N, a_test[i]);
    calc_scale_spherical(a_test[i], Lambda, &distortion[0]); // This returns the x_i
    dDgrowth = dGrowth_da_spc(a_test[i]);
    //printf("i = %d, distortion = (%g|%g|%g)\n", i, distortion[0], distortion[1], distortion[2]);
    fprintf(fp, "%d\t %g\t %g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t %g\n", i, a_test[i], distortion[0], distortion[1], distortion[2], distortion[3], distortion[4], distortion[5], Lambda[0], Lambda[1], Lambda[2], dDgrowth);
  }

  free_growth();
  free_spline();
  fclose(fp);
  return 0;
}
