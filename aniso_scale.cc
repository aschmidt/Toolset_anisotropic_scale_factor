/*******************************************************************************
 ** \copyright   This file is a standalone version to compute the evolution
 ** \copyright   of anisotropic expansion factors. Copyright (C) 2018 by Andreas Schmidt
 ** \copyright   (aschmidt@mpa-garching.mpg.de)
 ** \copyright   If you use this code please cite 2018MNRAS.479..162S
 ********************************************************************************/

/**
 * This file is part of aniso_scale.

    aniso_scale is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    aniso_scale is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with aniso_scale.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <gsl/gsl_spline.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_odeiv.h>

/**
  * Function to calculate the growth factor in the limit of LambdaCDM
  * use gsl interpolation gsl_interp_linear
  *
  */
#define DI_TABLE_LENGTH   10000
#define HUBBLE            3.2407789e-18   /* in h/sec */
#define KILOPARSEC        3.085678e21     /* kpc in cm */
#define VELOCITYUNIT      1.e5             /* km/s in cm/s */


static double Hubble0 = HUBBLE * KILOPARSEC/VELOCITYUNIT;  /* Hubble in internal gadget units = 0.1 */
static double Omega0 = 0.308, OmegaLambda = 0.692;

/* ##################### PROTOTYPES ################################### */
/* Define prototypes just in case */

double hubble_function(double a);
void hubble_function_aniso(double a, double Scale[3], double * hubble_a);
double EA(double a);
void get_name(const double Lambda[3], int N, const char filebase[], char buf[200]);

/* Growth function prototypes */
double Growth(double a);
double dGrowth_da(double a);
static double growth_integrant(double a, void *param);
void init_growth(double time_end, double Omega0);
void free_growth();

void calc_new_anisotropic_scale(double a, const double Lambda[3], double * dist);

/* Prototypes for Differential equation solving */
int diff_equation(double t, const double y[], double f[], void *params);
void initial_diff(const double aini, const double Lambda[3], double * y, int da);
void do_differential(const double aini, const double tend, const double Lambda[3], int da, void * da_params);

struct ode_params
{
 double beta[3];
};

/*######################### FUCTION DEFINITIONS ##################################*/

double hubble_function(double a)
{
  double hubble_a;

  hubble_a = Omega0 / (a * a * a) + (1 - Omega0 - OmegaLambda) / (a * a) + OmegaLambda;

  hubble_a = Hubble0 * sqrt(hubble_a);

  return hubble_a;
}

/**
  * New function to integrate the actual equation of motions
  * dp/dt = 4/3 \pi G \rho_{\mathrm{bg}} * (\delta + 3/2 \beta_{i} D(a)) a_{\mathrm{bg}}^2 x_{i}
  * dx_i/dt = p_{i}/a_{\mathrm{bg}}^2
  * \delta = 1/(x_{0} x_{1} x_{2}) -1
  * And rho_{\mathrm{bg}} is replaced by \Omege_{m,0}
  * we use gsl for the ode
  */

/**
  * For the gsl ode solver we set
  * y[0] = x_{0}, y[1] = x_{1}, y[2] = x_{3}
  * y[3] = p_{0}, y[4] = p_{1}, y[5] = p_{2}
  * y[6] = D(t), y[7] = a_{bg}
  *
  * Linear theory tidal field:
  * f[0] = y[3]/(y[7]*y[7]), f[1] = y[4]/(y[7]*y[7]), f[2] = y[5]/(y[7]*y[7])
  * f[3] = 0.5 * H_{0}^2 * Omega0 * ((1/(y[0]*y[1]*y[2]) -1) + 3./2. * beta_0 * y[6])*y[0]/y[7]
  * f[4] = 0.5 * H_{0}^2 * Omega0 * ((1/(y[0]*y[1]*y[2]) -1) + 3./2. * beta_1 * y[6])*y[1]/y[7]
  * f[5] = 0.5 * H_{0}^2 * Omega0 * ((1/(y[0]*y[1]*y[2]) -1) + 3./2. * beta_2 * y[6])*y[2]/y[7]
  * f[6] = da_{bg}/dt = H(a_bg) * a_{bg}
  *
  *	dp/dt = dp/da * da/dt = dp/da H(a)a = 4/3 \pi G \rho_{\mathrm{bg}} * (\delta + 3/2 \beta_{i} D(a)) a_{\mathrm{bg}}^2 x_{i}
  *
  * For another field change the variable double Tidalfield
  */
int diff_equation_da(double a, const double y[], double f[], void *params) // this is the same as below but with da instead of dt
{
  struct ode_params *params_ptr = (struct ode_params *)params;
  double beta[3];
  beta[0] = params_ptr->beta[0];
  beta[1] = params_ptr->beta[1];
  beta[2] = params_ptr->beta[2];
  //printf("beta = %g%g%g\n", beta[0], beta[1], beta[2]);
  //double beta[3] = *(double *)params; // get parameters from params in this case the beta values which are related to lambda
  double x[3] = { y[0], y[1], y[2] };
  double p[3] = { y[3], y[4], y[5] };
  //double D = y[6];
  double prefac = 1.5 * Hubble0 * Hubble0 * Omega0 / (hubble_function(a)*a*a);
  //printf("a = %g Prefac = %g H(a) = %g\n", a, prefac, hubble_function(a));

  // now we define the equations and store the result in f[]
  for(int i = 0; i < 3; i++)
  {
    f[i] = p[i]/(hubble_function(a) * a * a * a);  //dx_i/dt
    // Linear Theory tidal field:
    double Tidalfield=((1./(fabs(x[0]*x[1]*x[2])+1e-10) - 1.)/3. + 1./2. * beta[i]*Growth(a)/Growth(1.));
    //f[i + 3] = -prefac * x[i] * (1./(1.e-2+fabs(x[0]*x[1]*x[2])) - 1. + 3./2. * beta[i]*Growth(a)/Growth(1.)); //dp_i/dt
    f[i + 3] = -prefac * x[i] * Tidalfield; //dp_i/dt
  }
  //f[6] = hubble_function(a_bg) * a_bg;

  return GSL_SUCCESS;
}

int diff_equation(double t, const double y[], double f[], void *params)
{
  // first we change variables
  struct ode_params *params_ptr = (struct ode_params *)params;
  double beta[3];
  beta[0] = params_ptr->beta[0];
  beta[1] = params_ptr->beta[1];
  beta[2] = params_ptr->beta[2];
  //printf("beta = %g%g%g\n", beta[0], beta[1], beta[2]);
  //double beta[3] = *(double *)params; // get parameters from params in this case the beta values which are related to lambda
  double x[3] = { y[0], y[1], y[2] };
  double p[3] = { y[3], y[4], y[5] };
  //double D = y[6];
  double a_bg = y[6];
  double prefac = 0.5 * Hubble0 * Hubble0 * Omega0;
  //printf("a = %g Prefac = %g H(a) = %g\n", a_bg, prefac, hubble_function(a_bg));

  // now we define the equations and store the result in f[]
  for(int i = 0; i < 3; i++)
  {
    f[i] = p[i]/(a_bg * a_bg);  //dx_i/dt
    f[i + 3] = -prefac * x[i]/a_bg * (1./(fabs(x[0]*x[1]*x[2])+1.e-10) - 1. + 3./2. * beta[i]*Growth(a_bg)/Growth(1.)); //dp_i/dt
  }
  f[6] = hubble_function(a_bg) * a_bg;

  return GSL_SUCCESS;
}


/**
  * Define functions and variables for the growthfactor integration
  */

gsl_spline *growth_interp;
gsl_interp_accel *growth_acc;

/* Scaleing of the Hubble parameter */
// H(a) = E(a)*H0; E(a) =EA
double EA(double a)
{
    return sqrt(Omega0 / (a * a * a) + (1 - Omega0 - OmegaLambda) / (a * a) + OmegaLambda);
}

/**
  * This is the integrant for the growthfactor integration which is done below
  */
static double growth_integrant(double a, void *param)
{
  if(a < 1e-10)
    return pow(a, 3./2.);
  double ea = EA(a);
  return (1./(a*a*a*ea*ea*ea));
}

/**
  * Initialization for the integration of D(a)
  * First value is set to 0
  * The integration is performed using gsl and stored in the gsl_spline growth_interp
  * We store 1000 points in a
  */

void init_growth(double time_end, double Omega0)
{
  #define WORKSIZE 100000
  double result, abserr;
  gsl_function F;
  gsl_integration_workspace *workspace;
  workspace = gsl_integration_workspace_alloc(WORKSIZE);
  double Di[DI_TABLE_LENGTH];
  double ai[DI_TABLE_LENGTH];
  double time_end_tmp = time_end+0.1*time_end; // add more entries in the table
  /* do integration and save results into ai, Di */
  ai[0] = 0.;
  Di[0] = 0.;
  for(int i = 1; i < DI_TABLE_LENGTH; i++)
    {
      F.function = &growth_integrant;
      ai[i] = time_end_tmp / (DI_TABLE_LENGTH-1) * i;
      gsl_integration_qag(&F, 0., ai[i], 0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
      Di[i] = 5./2. * Omega0 * EA(ai[i]) * result;
      //printf("i = %d ai = %g, Di = %g\n", i, ai[i], Di[i]);
    }

  gsl_integration_workspace_free(workspace);

  growth_acc = gsl_interp_accel_alloc();
  growth_interp = gsl_spline_alloc(gsl_interp_cspline, DI_TABLE_LENGTH);
  gsl_spline_init(growth_interp, ai, Di, DI_TABLE_LENGTH);
}

/*
 * Free the integration variables and workspace
 */
void free_growth()
{
  gsl_spline_free(growth_interp);
  gsl_interp_accel_free(growth_acc);
}

/**
  * This function does the interpolation between different a values using a spline.
  */
double Growth(double a)
{
  return gsl_spline_eval(growth_interp, a, growth_acc);
}

/**
  * This calculated the derivative of D with respect to a
  */
double dGrowth_da(double a)
{ //gsl_spline_eval_deriv (const gsl_spline * spline, double x, gsl_interp_accel * acc)
  return gsl_spline_eval_deriv(growth_interp, a, growth_acc);
}

/*
 *  Compute anisotropic Hubble factor in internal units
 */
void hubble_function_aniso(double a, double Scale[3], double * hubble_a)
{
  //static double hubble_a[3];
  for(int i = 0; i < 3; i++)
  {
    hubble_a[i] = Omega0 / (Scale[i]*Scale[i]*Scale[i]) + (1 - Omega0 - OmegaLambda)/(Scale[i]*Scale[i]) + OmegaLambda;
    hubble_a[i] = Hubble0 * sqrt(hubble_a[i]);
  }
}

/* ############################## DIFFERENTIAL INTEGRATION ############################### */

/*
 * Initialize values for differential equation
 */

void initial_diff(const double aini, const double Lambda[3], double * y, int da)
{
  double growthratio = Growth(aini)/Growth(1.);
  //double dgrowthratio = dGrowth_da(aini)/(Growth(1.) * hubble_function(aini) * aini * aini * aini);
  double dgrowthratio = dGrowth_da(aini)/Growth(1.);
         dgrowthratio *= hubble_function(aini) * aini * aini * aini;
  if(da == 0)
    y[6] = aini;

  for(int i = 0; i < 3; i++)
  {
    y[i] = 1 - Lambda[i]*growthratio;
  }
  for(int i = 3; i < 6; i++)
  {
    y[i] = -Lambda[i - 3] * dgrowthratio;
  }
}

// This function does the actual integration

void do_differential(const double aini, const double tend, const double Lambda[3], int da, void * da_params)
{
  // We store the result of the ODE in six splines so we can interpolate using gsl
  int dimension;
  if(da == 0)
    dimension = 7;          // number of differential equations
  else if(da == 1)
    dimension = 6;

  double eps_abs = 1.e-8;     // absolute error
  double eps_rel = 1.e-10;    // relativ error

  // define the type of routine to make the ode integration
  const gsl_odeiv_step_type * type_ptr = gsl_odeiv_step_rkf45;
  // allocate and initialize the functions from gsl (stepper, control, evolution)
  gsl_odeiv_step *step_ptr = gsl_odeiv_step_alloc(type_ptr, dimension);
  gsl_odeiv_control *control_ptr = gsl_odeiv_control_y_new(eps_abs, eps_rel);
  gsl_odeiv_evolve *evolve_ptr = gsl_odeiv_evolve_alloc(dimension);

  gsl_odeiv_system ode_system;    // structure with the functions
  struct ode_params params;
  double trace = Lambda[0]+Lambda[1]+Lambda[2];
  for(int i = 0; i < 3; i++)
  {
    params.beta[i]= 2*Lambda[i] - 2./3. * trace;
    printf("i=%d -> beta = %g\n", i, params.beta[i]);
  }
  double y[dimension];
  double t, t_next, tmin, tmax, delta_t;
  double h = 1e-6;

  //Now load the values into the ode_system
  if(da == 0)
    ode_system.function = diff_equation;
  else if(da == 1)
    ode_system.function = diff_equation_da;
  else
  {
    printf("The value of da is %d but only 0,1 is allowed aborting\n", da);
    exit(-1);
  }
  ode_system.jacobian = NULL;
  ode_system.dimension = dimension;
  ode_system.params = &params;

  //Initialize values
  if(da == 0)
  {
    t = tmin = 0.;
    tmax = tend;
    delta_t = 0.01;
  }
  else if(da == 1)
  {
    int N = *(int *)da_params;
    printf("N input = %d\n", N);
    t = tmin = aini;
    tmax = tend;
	//delta_t = (tmax - tmin)/N;
    delta_t = (tend)/N; // To get the same steps as in the approximation
    //if(delta_t >= 1.e-3)
    //	tmax+=aini;
  }
  printf("Using tstart = %g, tend = %g -> delta t = %g, N = %d\n", tmin, tmax, delta_t, (int)((tmax-tmin)/delta_t));
  initial_diff(aini, Lambda, &y[0], da); //initial values
  //printf("%g %g %g %g %g %g %g %g %g\n", t, y[0], y[1], y[2], y[3], y[4], y[5], y[6], hubble_function(y[6]));
  FILE *fp_ode;
  char filebase2[] = "./ODE_INTEGRATION_DA_L";
  if(da == 0)
    strcpy(filebase2, "./ODE_INTEGRATION_L");

  char buf2[200];
  get_name(Lambda, (int)((tmax-tmin)/delta_t), &filebase2[0], buf2);
  printf("%s\n", buf2);
  fp_ode = fopen(buf2,"w");
  fprintf(fp_ode, "# t \t xi[3] \t pi[3] \t Lambda[3] = (%g,%g,%g), a, H(a)\n", Lambda[0], Lambda[1], Lambda[2]);
  if(da == 0)
    fprintf(fp_ode, "%g %g %g %g %g %g %g %g %g\n", t, y[0], y[1], y[2], y[3], y[4], y[5], y[6], hubble_function(y[6]));
  else if(da == 1)
    fprintf(fp_ode, "%g %g %g %g %g %g %g %g %g\n", t, y[0], y[1], y[2], y[3], y[4], y[5], t, hubble_function(t));
  char buf3[200];
  sprintf(buf3,"Growthfactor.txt");
  FILE *fp_grow;
  fp_grow=fopen(buf3,"w");
  fprintf(fp_grow, "%g %g %g\n",aini, Growth(aini),Growth(1.));
  // Now do the stepping
  for (t_next = tmin + delta_t; t_next <= tmax; t_next += delta_t)
  {
    while(t < t_next)
    {
      gsl_odeiv_evolve_apply(evolve_ptr, control_ptr, step_ptr, &ode_system, &t, t_next, &h, y);
    }
    if(da == 0)
      fprintf(fp_ode, "%g %g %g %g %g %g %g %g %g\n", t, y[0], y[1], y[2], y[3], y[4], y[5], y[6], hubble_function(y[6]));
    else if(da == 1){
      fprintf(fp_ode, "%g %g %g %g %g %g %g %g %g\n", t, y[0], y[1], y[2], y[3], y[4], y[5], t, hubble_function(t));
<<<<<<< HEAD
   /*
    if(da == 1)
	printf("t=%g, Prod(alpha)^-1[%g] - 1 - sum(lambda)[%g] = %g\n", t,1./(y[0]*y[1]*y[2]), (Lambda[0] + Lambda[1] + Lambda[2]),(1./(y[0]*y[1]*y[2]))-1-(Lambda[0] + Lambda[1] + Lambda[2]));
	*/
=======
      fprintf(fp_grow, "%g %g %g\n", t,Growth(t), Growth(1.));
      }
>>>>>>> 1ad0f308e8acb567421f5e3d950e4dc16f6b817b
  }
  fclose(fp_ode);
  fclose(fp_grow);
  /* all done; free up the gsl_odeiv stuff */
  gsl_odeiv_evolve_free (evolve_ptr);
  gsl_odeiv_control_free (control_ptr);
  gsl_odeiv_step_free (step_ptr);
}

/* ############# END OF DIFFERENTIAL INTEGRATION ############################### */

 /**
  *   anisotropic Scalefactor should scale with the linear growth factors which should be anisotropic as well
  *   PARAM:  a = background scale factor
  *           Lambda = Eigenvalues of the tidal tensor
  *           * dist = pointer where the values should be stored.
  */
void calc_new_anisotropic_scale(double a, const double Lambda[3], double * dist)
{
  double growth_now = Growth(1.);
  // All.Time = Background cosmological scale factor
  for(int i = 0; i < 3; i++)
  {
    dist[i] = 1. - Growth(a)/growth_now * Lambda[i]; // Lambda = Eigenvalue Deformation Tensor (Tidal field)
    dist[i+3] = -Lambda[i]/growth_now * dGrowth_da(a)*a*a*a*hubble_function(a);
    //printf("a_bg = %g -> New scale factor on axis %d = %g Growth factor(a_bg) = %g Lambda = %g\n",
    //        a, i, dist[i], Growth(a), Lambda[i]);
  }
}

/*##################### Aux helper function ###############################*/
void get_name(const double Lambda[3], int N, const char filebase[], char buf[200])
{
  int temp_lambda[3] = { (int)(Lambda[0]*100), (int)(Lambda[1]*100), (int)(Lambda[2]*100) };
  printf("%s -> %d%d%d\n", filebase, temp_lambda[0], temp_lambda[1], temp_lambda[2]);
  if(Lambda[0] >= 0 && Lambda[1] >= 0 && Lambda[2] >= 0)
    sprintf(buf,"%sp%dp%dp%d_N_%d.txt", filebase, temp_lambda[0], temp_lambda[1], temp_lambda[2], N);

  else if(Lambda[0] >= 0 && Lambda[1] >= 0 && Lambda[2] < 0)
    sprintf(buf,"%sp%dp%dn%d_N_%d.txt", filebase, temp_lambda[0], temp_lambda[1], -temp_lambda[2], N);

  else if(Lambda[0] >= 0 && Lambda[1] < 0 && Lambda[2] >= 0)
    sprintf(buf,"%sp%dn%dp%d_N_%d.txt", filebase, temp_lambda[0], -temp_lambda[1], temp_lambda[2], N);

  else if(Lambda[0] < 0 && Lambda[1] >= 0 && Lambda[2] >= 0)
    sprintf(buf,"%sn%dp%dp%d_N_%d.txt", filebase, -temp_lambda[0], temp_lambda[1], temp_lambda[2], N);

  else if(Lambda[0] < 0 && Lambda[1] < 0 && Lambda[2] >= 0)
    sprintf(buf,"%sn%dn%dp%d_N_%d.txt", filebase, -temp_lambda[0], -temp_lambda[1], temp_lambda[2], N);

  else if(Lambda[0] >= 0 && Lambda[1] < 0 && Lambda[2] < 0)
    sprintf(buf,"%sp%dn%dn%d_N_%d.txt", filebase, temp_lambda[0], -temp_lambda[1], -temp_lambda[2], N);

  else if(Lambda[0] < 0 && Lambda[1] >= 0 && Lambda[2] < 0)
    sprintf(buf,"%sn%dp%dn%d_N_%d.txt", filebase, -temp_lambda[0], temp_lambda[1], -temp_lambda[2], N);

  else if(Lambda[0] < 0 && Lambda[1] < 0 && Lambda[2] < 0)
    sprintf(buf,"%sn%dn%dn%d_N_%d.txt", filebase, -temp_lambda[0], -temp_lambda[1], -temp_lambda[2], N);
}

/*################################ MAIN FUNCTION #########################################*/
// Run the main program to get the approximate value for x_i

int main(int argc, char *argv[])
{
  int N;  // Number of steps (a's) we test
  double Lambda[3] = {0.01, -0.03, 0.01}; // Initialize to a certain set of Eigenvalues
  double distortion[6];
  double dDgrowth;
  double aini = 1./(100.); // z = 99

  printf("Running function %s with %d arguments\n", argv[0], argc);
  printf("Function %s has possible arguments: N (int), Lambda[0] (float), Lambda[1] (float), Lambda[2] (float), aini (float) aend (float)\n\n", argv[0]);

  #define TIMEBINS 29
  #define TIMEBASE (1<<TIMEBINS)

  //printf("TIMEBASE = %d\n", TIMEBASE);
  if(argc == 1)
    N = 100;
  else if(argc > 1)
    N = atoi(argv[1]);

  if(argc >= 5)
  {
    Lambda[0] = atof(argv[2]);
    Lambda[1] = atof(argv[3]);
    Lambda[2] = atof(argv[4]);
  }
  if(argc >= 6)
    aini = atof(argv[5]); // should be the same as 1/N

  double aend = 1.;
  if(argc >=7)
    aend = atof(argv[6]);

  double a_test=aini; // This are the a values of the background cosmology we use to interpolate

  printf("Lambda input %g%g%g\n", Lambda[0], Lambda[1], Lambda[2]);
  printf("aini = %g, aend = %g Hubble0 = %g\n", aini, aend, Hubble0);
  char lambda_str[100];
  init_growth(aend, Omega0);
  // Now open a file to write to
  FILE *fp;
  const char filebase[] = "background_cosmology_L_";
  char buf[200];
  get_name(Lambda, N, &filebase[0], buf);

  printf("%s\n", buf);
  printf("Growth z = 0 from z = 127: %g\n", Growth(1)/Growth(1/128.));
  //sprintf(buf,"./background_cosmology_N_%d.txt", N);
  fp = fopen(buf,"w");
  fprintf(fp, "# i \t a_bg \t Distortion [3] \t p_i[3] \t Lambda[3] = (%g%g%g), dD/da\n", Lambda[0], Lambda[1], Lambda[2]);
  double step = aend/N;
  int i = 0;
  // Main loop to do the interpolation
  while(a_test <= aend)
  {
    calc_new_anisotropic_scale(a_test, Lambda, &distortion[0]); // This returns the x_i
    dDgrowth = dGrowth_da(a_test);
    //printf("i = %d, distortion = (%g|%g|%g)\n", i, distortion[0], distortion[1], distortion[2]);
    fprintf(fp, "%d\t %g\t %g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t %g\n", i, a_test, distortion[0], distortion[1], distortion[2], distortion[3], distortion[4], distortion[5], Lambda[0], Lambda[1], Lambda[2], dDgrowth);
    a_test += step;
    i++;
  }
  // Now do the ode solving
  printf("Now solving the ODE\n");
  do_differential(aini, 10., Lambda, 0, NULL);
  printf("Doing differnetial equation with da\n");
  do_differential(aini, aend, Lambda, 1, &N);

  free_growth();
  fclose(fp);
  return 0;
}
