GSL_INCL   = -I$(GSL_HOME)/include
GSL_LIBS   = -L$(GSL_HOME)/lib

GSL_LIBS   += -lgsl -lgslcblas

OPT += -std=c++11

OBJS 	   = aniso_scale.o

CC = g++

EXEC = anisotropic_scale

LIBS += $(GSL_LIBS)
CFLAGS = $(OPT) $(GSL_INCL)
build: $(EXEC)

INCL = Makefile

.cc.o:
	$(CC) $(CFLAGS) -c $(OPT) -o "$@" "$<"

$(EXEC): $(OBJS)  
	$(CC) $(OPT) $(LIBS) $(OBJS) -o $(EXEC)

$(OBJS): $(INCL)

clean:
	rm -f $(OBJS) $(EXEC)
